<!DOCTYPE html>
<html lang="ru">
  <head>
  <link rel=stylesheet href="css_table.css">
  <link rel=stylesheet href="css_admin.css">
  <style>
  
p {
font-family: "Lucida Sans Unicode", "Lucida Grande", Serif;
font-style: italic;
font-size: 14px;
}

  </style>
  </head>

  <body>
  
<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.

$user = 'u16351';
$passme = '7947569';
$db = new PDO('mysql:host=localhost;dbname=u16351', $user, $passme, array(PDO::ATTR_PERSISTENT => true));

try {
    $stmt = $db->prepare("SELECT login, pass from admin");
    $stmt -> execute();
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
$admin = $stmt->fetchAll();
$log_admin = $admin[0]['login'];
$pass_admin = $admin[0]['pass'];

if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != $log_admin ||
    md5($_SERVER['PHP_AUTH_PW']) != $pass_admin) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Authorization required</h1>');
  exit();
}

print('<p>You have successfully logged in and see password protected data.</p>');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    

$user = 'u16351';
$passme = '7947569';
$db = new PDO('mysql:host=localhost;dbname=u16351', $user, $passme, array(PDO::ATTR_PERSISTENT => true));


try {
    $stmt = $db->prepare("SELECT COUNT(*) from application3");
    $stmt -> execute();
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
$rows_count = $stmt->fetchColumn();  // количество полученных строк


try {
    $stmt = $db->prepare("SELECT * from application3");
    $stmt -> execute();
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
$result = $stmt->fetchAll();


echo "<table><tr><th>Id</th><th>Login</th><th>Pass</th><th>Name</th><th>Email</th><th>Year of Birth</th><th>Abilitie(s)</th><th>Gender</th><th>Amount of limbs</th><th>Biography</th></tr>";
for ($i = 0 ; $i < $rows_count ; ++$i)
{
    echo "<tr>";
    echo "<td>";
    echo $result[$i]['id'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['login'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['pass'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['name'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['email'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['year'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['abilities'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['gender'];
    echo "</td>"; 
    echo "<td>";
    echo $result[$i]['limbs'];
    echo "</td>";
    echo "<td>";
    echo $result[$i]['bio'];
    echo "</td>";
}
echo "</table>";
}

else {
    
    if (!empty(strip_tags($_POST['delete']) and preg_match('/^[0-9]/',strip_tags($_POST['delete'])))) {
        $user = 'u16351';
        $passme = '7947569';
        $db = new PDO('mysql:host=localhost;dbname=u16351', $user, $passme, array(PDO::ATTR_PERSISTENT => true));
        
        try {
            $stmt = $db->prepare("DELETE FROM application3 WHERE id = ?");
            $stmt -> execute(array($_POST['delete']));
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
        
        try {
            $stmt = $db->prepare("SELECT COUNT(*) from application3");
            $stmt -> execute();
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
        $rows_count = $stmt->fetchColumn();  // количество полученных строк
        
        
        try {
            $stmt = $db->prepare("SELECT * from application3");
            $stmt -> execute();
        }
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
        $result = $stmt->fetchAll();
        
        
        echo "<table><tr><th>Id</th><th>Login</th><th>Pass</th><th>Name</th><th>Email</th><th>Year of Birth</th><th>Abilitie(s)</th><th>Gender</th><th>Amount of limbs</th><th>Biography</th></tr>";
        for ($i = 0 ; $i < $rows_count ; ++$i)
        {
            echo "<tr>";
            echo "<td>";
            echo $result[$i]['id'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['login'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['pass'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['name'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['email'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['year'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['abilities'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['gender'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['limbs'];
            echo "</td>";
            echo "<td>";
            echo $result[$i]['bio'];
            echo "</td>";
        }
        echo "</table>";
    }
    else print('INCORRECT VALUE');
    
}
?>

<br />
<form class="transparent" action="" method="POST">
<div class="form-inner">
<label>Enter id for delete:</label>
<br />
<input name="delete" />
<br />
<input type="submit" value="Delete" />
</div>
</form>

</body>
</html>
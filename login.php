<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
    session_destroy();
    header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>
    
    
    
<!DOCTYPE html>
<html lang="ru">
  <head>
  <link rel=stylesheet href="css_login.css">
  </head>
<body>
<form class="transparent" action="" method="post">
<div class="form-inner">
<label>Enter the login:</label>
  <input name="login" />
  <label>Enter the password:</label>
  <input name="pass" />
  <input type="submit" value="Enter" />
  </div>
</form>
</body>
</html>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $user = 'u16351';
  $passme = '7947569';
  $db = new PDO('mysql:host=localhost;dbname=u16351', $user, $passme, array(PDO::ATTR_PERSISTENT => true));

  try {
      $stmt = $db->prepare("SELECT id from application3 WHERE login = ? AND pass = ?");
      $stmt -> execute(array($_POST['login'],md5($_POST['pass'])));
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }
  $user_id = $stmt->fetchAll();
  if (!empty($user_id[0]['id'])){
  // Если все ок, то авторизуем пользователя.
  $_SESSION['login'] = $_POST['login'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = $user_id[0]['id'];
  $_SESSION['pass'] = md5($_POST['pass']);

  // Делаем перенаправление.
  header('Location: ./');
}
else {
    print('ERROR: INCORRECT LOGIN OR PASSWORD');
}
}